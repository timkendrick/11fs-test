import React from 'react';
import { getComponentName } from './lib/getComponentName';
import { getComponentType } from './lib/getComponentType';
import { isValueFactory } from './lib/isValueFactory';

/* tslint:disable: max-line-length */
export function withStaticProperties<V>(properties: V): <P extends {}>(component: React.ComponentType<P>) => React.ComponentType<P> & V;
export function withStaticProperties<V extends {}>(properties: ((existing: {}) => V)): <P>(component: React.ComponentType<P>) => (React.ComponentType<P> & V);
export function withStaticProperties<V1 extends {}, V2 extends {}>(properties: V1 | ((existing: {}) => V2)): <P>(component: React.ComponentType<P>) => (React.ComponentType<P> & V1) | (React.ComponentType<P> & V2);
/* tslint:disable: max-line-length */
export function withStaticProperties<V1 extends {}, V2 extends {}>(
  properties: V1 | ((component: React.ComponentType<any>) => V2),
): <P>(component: React.ComponentType<P>) => (React.ComponentType<P> & V1) | (React.ComponentType<P> & V2) {
  return <P>(component: React.ComponentType<P>): (React.ComponentType<P> & V1) | (React.ComponentType<P> & V2) => {
    const name = getComponentName(component);
    return Object.assign(
      (props: P) => React.createElement(component, props),
      name && { displayName: name },
      isValueFactory(properties) ? properties(getComponentType(component)) : properties,
    );
  };
}
