import { useContext } from 'react';
import { NavigationContext, NavigationParams, NavigationRoute, NavigationScreenProp } from 'react-navigation';

export function useNavigation<P extends NavigationParams = {}>(): NavigationScreenProp<NavigationRoute<P>, P> {
  return useContext(NavigationContext) as NavigationScreenProp<NavigationRoute<any>, any>;
}
