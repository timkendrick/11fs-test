import { useEffect, useRef, useState } from 'react';
import { RemoteData, remoteDataError, remoteDataLoading, remoteDataSuccess } from '../../types/RemoteData';

/* tslint:disable:max-line-length */
export function useRemoteDataProps<Result>(
  options: {
    placeholder?: undefined,
    params?: undefined,
    request: (params: undefined) => Promise<Result>,
  },
): RemoteData<Result, undefined>;
export function useRemoteDataProps<Result, Placeholder = Result | undefined>(
  options: {
    placeholder: (currentValue: RemoteData<Result, Placeholder> | undefined) => Placeholder,
    params?: undefined,
    request: (params: undefined) => Promise<Result>,
  },
): RemoteData<Result, Placeholder>;
export function useRemoteDataProps<Result, Params extends {} = {}>(
  options: {
    placeholder?: undefined,
    params: Params,
    request: (params: Params) => Promise<Result>,
  },
): RemoteData<Result, undefined>;
export function useRemoteDataProps<Result, Params extends {} = {}, Placeholder = Result | undefined>(
  options: {
    placeholder: (currentValue: RemoteData<Result, Placeholder> | undefined) => Placeholder,
    params: Params,
    request: (params: Params) => Promise<Result>,
  },
): RemoteData<Result, Placeholder>;
export function useRemoteDataProps<Result, Params extends {} = {}, Placeholder = Result | undefined>(
  options: {
    placeholder?: (currentValue: RemoteData<Result, Placeholder> | undefined) => Placeholder,
    params?: Params,
    request: (params: Params | undefined) => Promise<Result>,
  },
): RemoteData<Result, Placeholder>;
/* tslint:enable:max-line-length */
export function useRemoteDataProps<Result, Params extends {} = {}, Placeholder = Result | undefined>(
  options: {
    placeholder?: (currentValue: RemoteData<Result, Placeholder> | undefined) => Placeholder,
    params?: Params,
    request: (params: Params | undefined) => Promise<Result>,
  },
): RemoteData<Result, Placeholder> {
  const { placeholder: getPlaceholder, params, request } = options;
  const initialPlaceholder = remoteDataLoading(getPlaceholder ? getPlaceholder(undefined) : undefined as any);
  const [value, setValue] = useState<RemoteData<Result, Placeholder>>(initialPlaceholder);
  const activeOperation = useRef<Promise<Result> | undefined>(undefined);
  useEffect(
    function loadData() {
      if (getPlaceholder) { setValue(remoteDataLoading(getPlaceholder(value))); }
      const operation = request(params);
      activeOperation.current = operation;
      operation
        .then((value: Result) => {
          if (operation !== activeOperation.current) { return; }
          activeOperation.current = undefined;
          setValue(remoteDataSuccess(value));
        })
        .catch((error: Error) => {
          if (operation !== activeOperation.current) { return; }
          activeOperation.current = undefined;
          setValue(remoteDataError(error));
        });
    },
    params ? Object.values(params) : [],
  );
  return value;
}
