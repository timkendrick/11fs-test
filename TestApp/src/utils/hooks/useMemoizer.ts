import { useCallback } from 'react';
import { memoize } from '../lib/memoize';
import { memoizeLatest } from '../lib/memoizeLatest';

export interface MemoizerFn {
  <T extends Function>(fn: T): T;
}

export function useMemoizer<P extends {}, V extends {}>(options?: { latest?: boolean }): MemoizerFn {
  const onlyLatest = options && options.latest;
  return useCallback(createFunctionMemoizer(onlyLatest ? memoizeLatest : memoize), []) as MemoizerFn;
}

function createFunctionMemoizer(memoizer: MemoizerFn): <T extends Function>(fn: T) => T {
  const values = new WeakMap<Function, Function>();
  return <T extends Function>(fn: T): T => {
    if (values.has(fn)) { return values.get(fn) as T; }
    const memoized = memoizer(fn);
    values.set(fn, memoized);
    return memoized;
  };
}
