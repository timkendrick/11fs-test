import React from 'react';
import { HigherOrderComponent } from '../types/React';
import { createHigherOrderComponent } from './lib/createHigherOrderComponent';
import { getComponentName } from './lib/getComponentName';
import { isValueFactory } from './lib/isValueFactory';

/* tslint:disable: max-line-length */
export function withProps<P extends {}, V extends {}>(value: (props: P) => V): HigherOrderComponent<P, V>;
export function withProps<P extends {}, V extends {}>(value: V): HigherOrderComponent<P, P & V>;
export function withProps<P extends {}, V1 extends {}, V2 extends {}>(value: V1 | ((props: P) => V2)): HigherOrderComponent<P, (P & V1)> | HigherOrderComponent<P, V2>;
/* tslint:enable: max-line-length */
export function withProps<P extends {}, V1 extends {}, V2 extends {}>(
  value: V1 | ((props: P) => V2),
): HigherOrderComponent<P, P & V1> | HigherOrderComponent<P, V2> {
  return createHigherOrderComponent('withProps', (
    isValueFactory(value)
    ? (component: React.ComponentType<V2>): React.ComponentType<P> => (
      Object.assign((props: P) => React.createElement(component, value(props)), {
        displayName: getComponentName(component),
      })
    )
    : (component: React.ComponentType<P & V1>): React.ComponentType<P> => (
      Object.assign((props: P) => React.createElement(component, { ...props, ...value }), {
        displayName: getComponentName(component),
      })
    )
  ));
}
