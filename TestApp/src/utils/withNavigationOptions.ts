import { NavigationStackScreenOptions } from 'react-navigation';
import { HigherOrderComponent } from '../types/React';
import { createHigherOrderComponent } from './lib/createHigherOrderComponent';
import { withStaticProperties } from './withStaticProperties';

export function withNavigationOptions<P extends {}>(options: NavigationStackScreenOptions): HigherOrderComponent<P> {
  return createHigherOrderComponent('withNavigationOptions', withStaticProperties({ navigationOptions: options }));
}
