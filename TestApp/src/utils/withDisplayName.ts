import { HigherOrderComponent } from '../types/React';
import { getComponentName } from './lib/getComponentName';
import { isValueFactory } from './lib/isValueFactory';
import { withStaticProperties } from './withStaticProperties';

export function withDisplayName<P extends {}>(
  name: string | ((name: string | undefined) => string),
): HigherOrderComponent<P> {
  return (
    isValueFactory(name)
    ? withStaticProperties((component: React.ComponentType<any>) => ({
      displayName: name(getComponentName(component)),
    }))
    : withStaticProperties({ displayName: name })
  );
}
