import { RemoteError } from '../../types/RemoteError';

import { isRemoteDataError, isRemoteDataLoading, RemoteData } from '../../types/RemoteData';

export function remoteDataView<T extends {}, V, P, E extends RemoteError, O>(
  selector: (value: T) => RemoteData<V, P, E>,
  handlers: {
    error: (error: E, input: T) => O,
    loading: (placeholder: P, input: T) => O,
    success: (value: V, input: T) => O,
  },
): (input: T) => O {
  const { error, loading, success } = handlers;
  return (input: T): O => {
    const data = selector(input);
    if (isRemoteDataError(data)) { return error(data.error, input); }
    if (isRemoteDataLoading(data)) { return loading(data.placeholder, input); }
    return success(data.value, input);
  };
}
