const PENDING = Symbol('PENDING');

export function once<T extends Function>(fn: T): T {
  let value: any = PENDING;
  return ((...args: Array<any>): any => (value === PENDING ? (value = fn(...args)) : value)) as Function as T;
}
