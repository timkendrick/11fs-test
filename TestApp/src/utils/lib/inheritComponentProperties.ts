import { getComponentProperties } from './getComponentProperties';

export function inheritComponentProperties<
  T extends (React.ComponentType<any> | React.ExoticComponent<any>) & P,
  P extends {},
  V extends React.ComponentType<any>,
>(
  base: T,
): (value: V) => V & P {
  const properties = getComponentProperties<P>(base);
  return (value: V) => Object.assign(value, properties);
}
