export function isValueFactory<V, T extends Function>(value: V | T): value is T {
  return (typeof value === 'function');
}
