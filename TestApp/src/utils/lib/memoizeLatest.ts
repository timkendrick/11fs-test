const PENDING = Symbol('PENDING');

export function memoizeLatest<T extends Function>(fn: T): T {
  let previousArgs: Array<any> = PENDING as never;
  let previousValue: any = PENDING;
  return function evaluate(...args: Array<any>): any {
    if (
      (previousValue !== PENDING)
      && (args.length === previousArgs.length) && args.every((value, index) => previousArgs[index] === value)
    ) {
      return previousValue;
    }
    return (previousValue = fn(...(previousArgs = args)));
  } as Function as T;
}
