import { filterKeys } from './filterKeys';
import { getComponentType } from './getComponentType';

const INTRINSIC_FUNCTION_PROPERTY_NAMES: Set<PropertyKey> = new Set(Object.keys(() => {}));

export function getComponentProperties<V extends {}>(
  value: (React.ComponentType<any> | React.ExoticComponent<any>) & V,
): V {
  const type = getComponentType(value) as React.ComponentType<any> & V;
  return getFunctionProperties(type);
}

function getFunctionProperties<V extends {}>(value: Function & V): V {
  return filterKeys(value, (key) => !isIntrinsicFunctionProperty(key));
}

function isIntrinsicFunctionProperty(value: PropertyKey): boolean {
  return INTRINSIC_FUNCTION_PROPERTY_NAMES.has(value);
}
