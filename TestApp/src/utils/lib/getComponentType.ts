export function getComponentType<V extends {}>(
  value: (React.ComponentType<any> | React.ExoticComponent<any>) & V,
): React.ComponentType<any> {
  if (!isExoticComponent(value)) { return value; }
  if (isForwardRefComponent(value)) { return value.render; }
  if (isMemoComponent(value)) { return value.type; }
  throw new Error('Invalid component type');
}

function isExoticComponent<P extends {}>(
  value: React.ComponentType<P> | React.ExoticComponent<P>,
): value is React.ExoticComponent<P> {
  return (typeof value !== 'function');
}

function isForwardRefComponent<P extends {}>(
  value: React.ExoticComponent<P>,
): value is React.ForwardRefExoticComponent<P> & {
  render: React.ComponentType<P>,
} {
  return (value.$$typeof === Symbol.for('react.forward_ref'));
}

function isMemoComponent<P extends {}>(
  value: React.ExoticComponent<React.ComponentPropsWithRef<React.ComponentType<P>>>,
): value is React.MemoExoticComponent<React.ComponentType<P>> {
  return (value.$$typeof === Symbol.for('react.memo'));
}
