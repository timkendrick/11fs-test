export function compose<T1, V>(
  fn: (value: T1) => V,
): (value: T1) => V;
export function compose<T1, T2, V>(
  fn1: (value: T1) => T2,
  fn2: (value: T2) => V,
): (value: T1) => V;
export function compose<T1, T2, T3, V>(
  fn1: (value: T1) => T2,
  fn2: (value: T2) => T3,
  fn3: (value: T3) => V,
): (value: T1) => V;
export function compose<T1, T2, T3, T4, V>(
  fn1: (value: T1) => T2,
  fn2: (value: T2) => T3,
  fn3: (value: T3) => T4,
  fn4: (value: T4) => V,
): (value: T1) => V;
export function compose<T1, T2, T3, T4, T5, V>(
  fn1: (value: T1) => T2,
  fn2: (value: T2) => T3,
  fn3: (value: T3) => T4,
  fn4: (value: T4) => T5,
  fn5: (value: T5) => V,
): (value: T1) => V;
export function compose<T1, T2, T3, T4, T5, T6, V>(
  fn1: (value: T1) => T2,
  fn2: (value: T2) => T3,
  fn3: (value: T3) => T4,
  fn4: (value: T4) => T5,
  fn5: (value: T5) => T6,
  fn6: (value: T6) => V,
): (value: T1) => V;
export function compose<T1, T2, T3, T4, T5, T6, T7, V>(
  fn1: (value: T1) => T2,
  fn2: (value: T2) => T3,
  fn3: (value: T3) => T4,
  fn4: (value: T4) => T5,
  fn5: (value: T5) => T6,
  fn6: (value: T6) => T7,
  fn7: (value: T7) => V,
): (value: T1) => V;
export function compose<T1, T2, T3, T4, T5, T6, T7, T8, V>(
  fn1: (value: T1) => T2,
  fn2: (value: T2) => T3,
  fn3: (value: T3) => T4,
  fn4: (value: T4) => T5,
  fn5: (value: T5) => T6,
  fn6: (value: T6) => T7,
  fn7: (value: T7) => T8,
  fn8: (value: T8) => V,
): (value: T1) => V;
export function compose<T1, T2, T3, T4, T5, T6, T7, T8, T9, V>(
  fn1: (value: T1) => T2,
  fn2: (value: T2) => T3,
  fn3: (value: T3) => T4,
  fn4: (value: T4) => T5,
  fn5: (value: T5) => T6,
  fn6: (value: T6) => T7,
  fn7: (value: T7) => T8,
  fn8: (value: T8) => T9,
  fn9: (value: T9) => V,
): (value: T1) => V;
export function compose<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, V>(
  fn1: (value: T1) => T2,
  fn2: (value: T2) => T3,
  fn3: (value: T3) => T4,
  fn4: (value: T4) => T5,
  fn5: (value: T5) => T6,
  fn6: (value: T6) => T7,
  fn7: (value: T7) => T8,
  fn8: (value: T8) => T9,
  fn9: (value: T9) => T10,
  fn10: (value: T10) => V,
): (value: T1) => V;
export function compose<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, V>(
  fn1: (value: T1) => T2,
  fn2: (value: T2) => T3,
  fn3: (value: T3) => T4,
  fn4: (value: T4) => T5,
  fn5: (value: T5) => T6,
  fn6: (value: T6) => T7,
  fn7: (value: T7) => T8,
  fn8: (value: T8) => T9,
  fn9: (value: T9) => T10,
  fn10: (value: T10) => T11,
  fn11: (value: T11) => V,
): (value: T1) => V;
export function compose<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, V>(
  fn1: (value: T1) => T2,
  fn2: (value: T2) => T3,
  fn3: (value: T3) => T4,
  fn4: (value: T4) => T5,
  fn5: (value: T5) => T6,
  fn6: (value: T6) => T7,
  fn7: (value: T7) => T8,
  fn8: (value: T8) => T9,
  fn9: (value: T9) => T10,
  fn10: (value: T10) => T11,
  fn11: (value: T11) => T12,
  fn12: (value: T12) => V,
): (value: T1) => V;
export function compose<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, V>(
  fn1: (value: T1) => T2,
  fn2: (value: T2) => T3,
  fn3: (value: T3) => T4,
  fn4: (value: T4) => T5,
  fn5: (value: T5) => T6,
  fn6: (value: T6) => T7,
  fn7: (value: T7) => T8,
  fn8: (value: T8) => T9,
  fn9: (value: T9) => T10,
  fn10: (value: T10) => T11,
  fn11: (value: T11) => T12,
  fn12: (value: T12) => T13,
  fn13: (value: T13) => V,
): (value: T1) => V;
export function compose<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, V>(
  fn1: (value: T1) => T2,
  fn2: (value: T2) => T3,
  fn3: (value: T3) => T4,
  fn4: (value: T4) => T5,
  fn5: (value: T5) => T6,
  fn6: (value: T6) => T7,
  fn7: (value: T7) => T8,
  fn8: (value: T8) => T9,
  fn9: (value: T9) => T10,
  fn10: (value: T10) => T11,
  fn11: (value: T11) => T12,
  fn12: (value: T12) => T13,
  fn13: (value: T13) => T14,
  fn14: (value: T14) => V,
): (value: T1) => V;
export function compose<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, V>(
  fn1: (value: T1) => T2,
  fn2: (value: T2) => T3,
  fn3: (value: T3) => T4,
  fn4: (value: T4) => T5,
  fn5: (value: T5) => T6,
  fn6: (value: T6) => T7,
  fn7: (value: T7) => T8,
  fn8: (value: T8) => T9,
  fn9: (value: T9) => T10,
  fn10: (value: T10) => T11,
  fn11: (value: T11) => T12,
  fn12: (value: T12) => T13,
  fn13: (value: T13) => T14,
  fn14: (value: T14) => T15,
  fn15: (value: T15) => V,
): (value: T1) => V;
export function compose<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, V>(
  fn1: (value: T1) => T2,
  fn2: (value: T2) => T3,
  fn3: (value: T3) => T4,
  fn4: (value: T4) => T5,
  fn5: (value: T5) => T6,
  fn6: (value: T6) => T7,
  fn7: (value: T7) => T8,
  fn8: (value: T8) => T9,
  fn9: (value: T9) => T10,
  fn10: (value: T10) => T11,
  fn11: (value: T11) => T12,
  fn12: (value: T12) => T13,
  fn13: (value: T13) => T14,
  fn14: (value: T14) => T15,
  fn15: (value: T15) => T16,
  fn16: (value: T16) => V,
): (value: T1) => V;
export function compose(...fns: Array<(value: any) => any>): (value: any) => any;
export function compose(...fns: Array<(value: any) => any>): (value: any) => any {
  return fns.reduce((acc, fn) => (x) => fn(acc(x)));
}
