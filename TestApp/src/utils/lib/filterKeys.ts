export function filterKeys<T extends { [key: string]: any }>(
  value: T,
  predicate: (key: PropertyKey) => boolean,
): { [K in keyof T]: T[K] } {
  return Object.keys(value).reduce(
    (acc, key) => {
      if (!predicate(key)) { return acc; }
      acc[key] = value[key];
      return acc;
    },
    {} as { [K in keyof T]: T[K] },
  );
}
