const PENDING = Symbol('PENDING');

export function memoize<T extends Function>(fn: T): T {
  const objectCache = new WeakMap();
  const primitiveCache = new Map();
  let value: any = PENDING;
  return function evaluate(...args: Array<any>): any {
    if (args.length === 0) { return (value !== PENDING ? value : (value = fn())); }
    const [arg, ...remainingArgs] = args;
    const cache = isPrimitive(arg) ? primitiveCache : objectCache;
    if (cache.has(arg)) { return cache.get(arg)(...remainingArgs); }
    const memoized = memoize(fn.bind(null, arg));
    cache.set(arg, memoized);
    return memoized(...remainingArgs);
  } as Function as T;
}

function isPrimitive(value: any): value is undefined | null | boolean | number | string | symbol {
  switch (typeof value) {
    case 'undefined':
    case 'boolean':
    case 'number':
    case 'string':
    case 'symbol':
      return true;
    default:
      return (value !== null);
  }
}
