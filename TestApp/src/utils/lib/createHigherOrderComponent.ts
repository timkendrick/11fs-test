import { HigherOrderComponent } from '../../types/React';
import { withDisplayName } from '../withDisplayName';
import { compose } from './compose';
import { inheritComponentProperties } from './inheritComponentProperties';

export function createHigherOrderComponent<T extends HigherOrderComponent<any, any>>(
  name: string,
  implementation: T,
): T {
  const withComponentName = compose(withDisplayName((innerName) => `${name}(${innerName})`), implementation);
  return ((component: React.ComponentType<any>): React.ComponentType<any> => inheritComponentProperties(component)(
    withComponentName(component),
  )) as T;
}
