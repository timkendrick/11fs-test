import { getComponentType } from './getComponentType';

export function getComponentName(value: React.ComponentType<any>): string | undefined {
  const type = getComponentType(value);
  return type.displayName || type.name || undefined;
}
