import { NavigationParams } from 'react-navigation';
import { HigherOrderComponent } from '../types/React';
import { useNavigation } from './hooks/useNavigation';
import { createHigherOrderComponent } from './lib/createHigherOrderComponent';
import { withProps } from './withProps';

export function withNavigationParams<P extends {}, V extends {}>(
  factory: (params: NavigationParams | undefined) => V,
): HigherOrderComponent<P, P & V> {
  return createHigherOrderComponent('withNavigationParams', withProps((props: P): P & V => {
    const navigation = useNavigation();
    return {
      ...props,
      ...factory(navigation.state.params),
    };
  }));
}
