import { TextStyle, ViewStyle } from 'react-native';

export const BACKGROUND_COLOR_DEFAULT: string = 'rgb(250, 250, 250)';
export const BORDER_COLOR_SECTION_SEPARATOR: string = 'rgb(210, 210, 210)';
export const BORDER_COLOR_LIST_SEPARATOR: string = 'rgb(220, 220, 220)';

export const TEXT_COLOR_DEFAULT: string = '#111';
export const TEXT_COLOR_ERROR: string = '#C66';

export const TEXT_SIZE_DEFAULT: number = 16;
export const TEXT_SIZE_TITLE: number = 24;
export const TEXT_SIZE_SUBTITLE: number = 20;

export const PADDING_DEFAULT: number = 10;
export const PADDING_SUBTITLE: number = 2;

export const BORDER_STYLE_SECTION_SEPARATOR: ViewStyle = {
  borderBottomWidth: 1,
  borderBottomColor: BORDER_COLOR_SECTION_SEPARATOR,
};

export const PAGE: ViewStyle = {
  padding: PADDING_DEFAULT,
  backgroundColor: BACKGROUND_COLOR_DEFAULT,
};

export const BODY_TEXT: TextStyle = {
  fontSize: 14,
};

export const BOLD_TEXT: TextStyle = {
  ...BODY_TEXT,
  fontWeight: 'bold',
};

export const SCROLL_CONTENT: ViewStyle = {
  marginBottom: 5 * PADDING_DEFAULT,
};

export const SEARCH_INPUT: TextStyle = {
  height: 40,
  padding: PADDING_DEFAULT,
};

export const LIST_ITEM_SEPARATOR: ViewStyle = {
  borderBottomWidth: 1,
  borderBottomColor: BORDER_COLOR_LIST_SEPARATOR,
};

export const SECTION: ViewStyle = {
};

export const SECTION_TITLE: TextStyle = {
  fontSize: TEXT_SIZE_SUBTITLE,
  fontWeight: 'bold',
  marginBottom: 5,
};

export const LIST: ViewStyle = {
  backgroundColor: BACKGROUND_COLOR_DEFAULT,
};

export const HEADER: ViewStyle = {
  marginBottom: 10,
};

export const SECTION_SEPARATOR: ViewStyle = {
  marginTop: 2 * PADDING_DEFAULT,
  marginBottom: 1.5 * PADDING_DEFAULT,
  ...BORDER_STYLE_SECTION_SEPARATOR,
};

export const HEADER_TITLE: TextStyle = {
  fontSize: TEXT_SIZE_TITLE,
  fontWeight: 'bold',
};

export const HEADER_SUBTITLE: TextStyle = {
  fontSize: TEXT_SIZE_SUBTITLE,
};

export const LIST_ITEM: ViewStyle = {
  padding: PADDING_DEFAULT,
};

export const LIST_ITEM_TITLE: TextStyle = {
  fontWeight: 'bold',
  fontSize: TEXT_SIZE_DEFAULT,
  color: TEXT_COLOR_DEFAULT,
  marginBottom: PADDING_SUBTITLE,
};

export const LIST_ITEM_SUBTITLE: TextStyle = {
  fontSize: TEXT_SIZE_DEFAULT,
  color: TEXT_COLOR_DEFAULT,
};
