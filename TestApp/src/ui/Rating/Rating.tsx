import React from 'react';
import { StyleProp, Text, ViewStyle } from 'react-native';

import styles from './Rating.styles';

export interface RatingProps {
  style?: StyleProp<ViewStyle>;
  value: number;
}

export const Rating = React.memo(function Rating(props: RatingProps): JSX.Element {
  const { style } = props;
  return (
    <Text style={[styles.root, style]}>{getRatingContent(props.value)}</Text>
  );
});

function getRatingContent(value: number): React.ReactChild {
  return Array.from({ length: Math.round(value) }, () => '⭐️').join('');
}
