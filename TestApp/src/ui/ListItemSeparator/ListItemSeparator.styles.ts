import { StyleSheet } from 'react-native';
import { LIST_ITEM_SEPARATOR } from '../../styles';

export default StyleSheet.create({
  root: LIST_ITEM_SEPARATOR,
});
