import React from 'react';
import { View } from 'react-native';
import styles from './ListItemSeparator.styles';

export interface ListItemSeparatorProps {
}

export const ListItemSeparator = React.memo(function ListItemSeparator(props: LoadingIndicatorProps): JSX.Element {
  return (
    <View style={styles.root} />
  );
});
