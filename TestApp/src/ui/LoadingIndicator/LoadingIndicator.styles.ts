import { StyleSheet } from 'react-native';
import { PADDING_DEFAULT, SCROLL_CONTENT } from '../../styles';

export default StyleSheet.create({
  root: {
    ...SCROLL_CONTENT,
    marginTop: 2 * PADDING_DEFAULT,
  },
});
