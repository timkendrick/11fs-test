import React from 'react';
import { ActivityIndicator, StyleProp, ViewStyle } from 'react-native';
import styles from './LoadingIndicator.styles';

export interface LoadingIndicatorProps {
  style?: StyleProp<ViewStyle>;
}

export const LoadingIndicator = React.memo(function LoadingIndicator(props: LoadingIndicatorProps): JSX.Element {
  const { style } = props;
  return (
    <ActivityIndicator style={[styles.root, style]} />
  );
});
