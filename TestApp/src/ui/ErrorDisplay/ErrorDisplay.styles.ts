import { StyleSheet } from 'react-native';
import { TEXT_COLOR_ERROR } from '../../styles';

export default StyleSheet.create({
  root: {},
  text: {
    color: TEXT_COLOR_ERROR,
  },
});
