import React from 'react';
import { StyleProp, Text, View, ViewStyle } from 'react-native';

import styles from './ErrorDisplay.styles';

export interface ErrorDisplayProps {
  style?: StyleProp<ViewStyle>;
  error: string | SerializedError | Error;
}

interface SerializedError {
  message: string;
}

export const ErrorDisplay = React.memo(function ErrorDisplay(props: ErrorDisplayProps): JSX.Element {
  const { style, error } = props;
  return (
    <View style={[styles.root, style]}>
      <Text style={styles.text}>{getErrorText(error)}</Text>
    </View>
  );
});

function getErrorText(value: string | SerializedError | Error): string {
  if (typeof value === 'string') { return value; }
  return value.message;
}
