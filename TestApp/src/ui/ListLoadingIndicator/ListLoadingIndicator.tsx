import React from 'react';
import { StyleProp, ViewStyle } from 'react-native';
import { LoadingIndicator } from '../LoadingIndicator';
import styles from './ListLoadingIndicator.styles';

export interface ListLoadingIndicatorProps {
  style?: StyleProp<ViewStyle>;
}

export const ListLoadingIndicator = React.memo(function ListLoadingIndicator(
  props: ListLoadingIndicatorProps,
): JSX.Element {
  const { style } = props;
  return (
    <LoadingIndicator style={[styles.root, style]} />
  );
});
