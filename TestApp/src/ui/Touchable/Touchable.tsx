import React from 'react';
import { GestureResponderEvent, StyleProp, TouchableOpacity, ViewStyle } from 'react-native';
import styles from './Touchable.styles';

export interface TouchableProps {
  style?: StyleProp<ViewStyle>;
  testID?: string;
  disabled?: boolean;
  onPress?: (event: GestureResponderEvent) => void;
  onPressIn?: (event: GestureResponderEvent) => void;
  onPressOut?: (event: GestureResponderEvent) => void;
  onLongPress?: (event: GestureResponderEvent) => void;
  children: React.ReactNode;
}

export const Touchable = React.memo(function Touchable(props: TouchableProps): JSX.Element {
  const { style, testID, disabled, onPress, onPressIn, onPressOut, onLongPress, children } = props;
  return (
    <TouchableOpacity
      style={[styles.root, style]}
      testID={testID}
      disabled={disabled}
      onPress={onPress}
      onPressIn={onPressIn}
      onPressOut={onPressOut}
      onLongPress={onLongPress}
    >
      {children}
    </TouchableOpacity>
  );
});
