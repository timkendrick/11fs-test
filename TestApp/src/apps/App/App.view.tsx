import { createAppContainer, createStackNavigator } from 'react-navigation';
import ROUTES, { INITIAL_ROUTE } from './App.routes';

export const AppView = createAppContainer(createStackNavigator(ROUTES, {
  initialRouteName: INITIAL_ROUTE,
}));
