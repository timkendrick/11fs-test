import { DetailScreen } from '../../screens/DetailScreen';
import { HomeScreen } from '../../screens/HomeScreen';

export const ROUTE_HOME = 'home';
export const ROUTE_DETAIL = 'detail';

const ROUTES = {
  [ROUTE_HOME]: HomeScreen,
  [ROUTE_DETAIL]: DetailScreen,
};

export const INITIAL_ROUTE = ROUTE_HOME;

export default ROUTES;
