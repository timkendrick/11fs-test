export * from './App.view';
export * from './App.routes';

import { AppView } from './App.view';
export const App = AppView;
