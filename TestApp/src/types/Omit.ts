export type Omit<V extends {}, K extends keyof V> = Pick<V, Exclude<keyof V, K>>;
