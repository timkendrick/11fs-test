export interface HigherOrderComponent<OuterProps extends {}, InnerProps extends {} = OuterProps> {
  (component: React.ComponentType<InnerProps>): React.ComponentType<OuterProps>;
}
