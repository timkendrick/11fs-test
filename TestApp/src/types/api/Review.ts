import { User } from './User';
import { Uuid } from './Uuid';

export interface Review {
  id: Uuid;
  user: User;
  rating: number;
  title: string;
  body: string;
}
