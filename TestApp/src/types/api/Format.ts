import { Uuid } from './Uuid';

export interface Format {
  id: Uuid;
  name: string;
}
