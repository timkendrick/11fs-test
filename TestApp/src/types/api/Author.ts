import { Uuid } from './Uuid';

export interface Author {
  id: Uuid;
  name: string;
}
