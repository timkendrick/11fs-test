import { Author } from './Author';
import { Edition } from './Edition';
import { Publisher } from './Publisher';
import { Review } from './Review';
import { Uuid } from './Uuid';

export interface Book {
  id: Uuid;
  title: string;
  author: Author;
  description: string;
  publisher: Publisher;
  editions: Array<Edition>;
  reviews: Array<Review>;
}
