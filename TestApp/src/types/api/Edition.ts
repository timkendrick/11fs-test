import { Format } from './Format';

export interface Edition {
  format: Format;
  isbn: string;
}
