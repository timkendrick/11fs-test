import { Uuid } from './Uuid';

export interface Publisher {
  id: Uuid;
  name: string;
}
