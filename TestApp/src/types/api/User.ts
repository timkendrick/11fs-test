import { Uuid } from './Uuid';

export interface User {
  id: Uuid;
  name: string;
}
