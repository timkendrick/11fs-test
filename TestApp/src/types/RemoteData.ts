import { RemoteError } from './RemoteError';

export type RemoteData<V, P = V | undefined, E extends RemoteError = RemoteError> = (
  RemoteDataLoading<P> | RemoteDataSuccess<V> | RemoteDataError<E>
);

export interface RemoteDataLoading<P> {
  loading: true;
  error: undefined;
  placeholder: P;
}
export function remoteDataLoading<P>(placeholder: P): RemoteDataLoading<P> {
  return {
    loading: true,
    error: undefined,
    placeholder,
  };
}
export function isRemoteDataLoading<P>(value: RemoteData<any, P>): value is RemoteDataLoading<P> {
  return Boolean(value.loading);
}

export interface RemoteDataSuccess<V> {
  loading: false;
  error: undefined;
  value: V;
}
export function remoteDataSuccess<V>(value: V): RemoteDataSuccess<V> {
  return {
    loading: false,
    error: undefined,
    value,
  };
}
export function isRemoteDataSuccess<V>(value: RemoteData<V>): value is RemoteDataSuccess<V> {
  return !value.loading && !value.error;
}

export interface RemoteDataError<E extends RemoteError = RemoteError> {
  loading: false;
  error: E;
}
export function remoteDataError<E>(error: E): RemoteDataError<E> {
  return {
    loading: false,
    error,
  };
}
export function isRemoteDataError<E extends RemoteError>(value: RemoteData<any, any, E>): value is RemoteDataError<E> {
  return Boolean(value.error);
}
