export interface RemoteError {
  message: string;
}
