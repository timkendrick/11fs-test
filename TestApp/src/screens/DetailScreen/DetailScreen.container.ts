import { getBook } from '../../data/api';
import { Book } from '../../types/api/Book';
import { HigherOrderComponent } from '../../types/React';
import { useNavigation } from '../../utils/hooks/useNavigation';
import { useRemoteDataProps } from '../../utils/hooks/useRemoteDataProps';
import { compose } from '../../utils/lib/compose';
import { withNavigationOptions } from '../../utils/withNavigationOptions';
import { withProps } from '../../utils/withProps';
import { DetailScreenViewProps } from './DetailScreen.view';

export interface DetailScreenContainerProps {
}

export const DetailScreenContainer: HigherOrderComponent<DetailScreenContainerProps, DetailScreenViewProps> = compose(
  withNavigationOptions({
    title: 'Book details',
  }),
  withProps(assignViewProps),
);

function assignViewProps(props: DetailScreenContainerProps): DetailScreenViewProps {
  const navigation = useNavigation<{
    id: string;
    placeholder?: Book;
  }>();
  const book = useRemoteDataProps({
    placeholder: () => navigation.getParam('placeholder'),
    params: {
      id: navigation.getParam('id'),
    },
    request: (params: { id: string }) => getBook(params.id),
  });
  return {
    book,
  };
}
