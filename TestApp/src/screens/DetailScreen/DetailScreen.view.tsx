import React from 'react';
import { View } from 'react-native';
import { BookDetail } from '../../components/BookDetail';
import { Book } from '../../types/api/Book';
import { RemoteData } from '../../types/RemoteData';
import styles from './DetailScreen.styles';

export interface DetailScreenViewProps {
  book: RemoteData<Book>;
}

export const DetailScreenView = React.memo(function DetailScreenView(props: DetailScreenViewProps): JSX.Element {
  const { book } = props;
  return (
    <View style={styles.root}>
      <BookDetail style={styles.content} book={book} />
    </View>
  );
});
