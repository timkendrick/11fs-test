import { DetailScreenContainer, DetailScreenContainerProps } from './DetailScreen.container';
import { DetailScreenView } from './DetailScreen.view';

export * from './DetailScreen.container';
export * from './DetailScreen.view';
export const DetailScreen = DetailScreenContainer(DetailScreenView);
export interface DetailScreenProps extends DetailScreenContainerProps {}
