import { HomeScreenContainer, HomeScreenContainerProps } from './HomeScreen.container';
import { HomeScreenView } from './HomeScreen.view';

export * from './HomeScreen.container';
export * from './HomeScreen.view';
export const HomeScreen = HomeScreenContainer(HomeScreenView);
export interface HomeScreenProps extends HomeScreenContainerProps {}
