import { useCallback, useState } from 'react';
import { ROUTE_DETAIL } from '../../apps/App';
import { getBooks } from '../../data/api';
import { Book } from '../../types/api/Book';
import { HigherOrderComponent } from '../../types/React';
import {
  isRemoteDataError,
  isRemoteDataLoading,
  isRemoteDataSuccess,
  RemoteData,
  remoteDataLoading,
  remoteDataSuccess,
} from '../../types/RemoteData';
import { useNavigation } from '../../utils/hooks/useNavigation';
import { useRemoteDataProps } from '../../utils/hooks/useRemoteDataProps';
import { compose } from '../../utils/lib/compose';
import { noop } from '../../utils/lib/noop';
import { once } from '../../utils/lib/once';
import { withNavigationOptions } from '../../utils/withNavigationOptions';
import { withProps } from '../../utils/withProps';
import { HomeScreenViewProps } from './HomeScreen.view';

const DEFAULT_INITIAL_PAGE_SIZE = 50;
const DEFAULT_PAGE_SIZE_INCREMENT = 100;

export interface HomeScreenContainerProps {
  initialPageSize?: number;
  pageSizeIncrement?: number;
}

interface PaginationProps {
  pageSize: number;
  setPageSize: (value: number | ((previousValue: number) => number)) => void;
}

interface RemoteDataProps {
  books: RemoteData<GetBooksResponse>;
}

interface GetBooksResponse {
  items: Array<Book>;
  count: number;
}

export const HomeScreenContainer: HigherOrderComponent<HomeScreenContainerProps, HomeScreenViewProps> = compose(
  withNavigationOptions({
    title: 'All books',
  }),
  withProps(compose(
    assignPaginationProps,
    assignRemoteDataProps,
    assignViewProps,
  )),
);

function assignPaginationProps<P extends HomeScreenContainerProps>(props: P): P & PaginationProps {
  const { initialPageSize = DEFAULT_INITIAL_PAGE_SIZE } = props;
  const [pageSize, setPageSize] = useState(initialPageSize);
  return {
    ...props,
    pageSize,
    setPageSize,
  };
}

function assignRemoteDataProps<P extends PaginationProps>(props: P): P & RemoteDataProps {
  const { pageSize } = props;
  const books = useRemoteDataProps({
    placeholder: (currentValue: RemoteData<GetBooksResponse> | undefined) => getRemoteDataPlaceholder(currentValue),
    params: { pageSize },
    request: ({ pageSize }) => getBooks({ offset: 0, length: pageSize }),
  });
  return {
    ...props,
    books,
  };
}

function getRemoteDataPlaceholder(value: RemoteData<GetBooksResponse> | undefined): GetBooksResponse | undefined {
  if (!value || isRemoteDataError(value)) { return undefined; }
  if (isRemoteDataLoading(value)) { return value.placeholder; }
  return value.value;
}

function assignViewProps(
  props: HomeScreenContainerProps & PaginationProps & RemoteDataProps,
): HomeScreenViewProps {
  const { books, pageSizeIncrement = DEFAULT_PAGE_SIZE_INCREMENT, pageSize, setPageSize } = props;
  const hasAdditionalItems = getHasAdditionalItems(books, pageSize);
  const isScrollLoadingDisabled = (isRemoteDataLoading(books) || !hasAdditionalItems);
  const navigation = useNavigation();
  const onScrollToEnd = useCallback(
    (isScrollLoadingDisabled ? noop : once(() => { setPageSize((value) => value + pageSizeIncrement); })),
    [pageSizeIncrement, isScrollLoadingDisabled],
  );
  const onSelectBook = useCallback(
    (book: Book) => navigation.navigate(ROUTE_DETAIL, { id: book.id, placeholder: book }),
    [navigation],
  );
  return {
    books: extractPaginatedItems(books),
    hasAdditionalItems,
    onScrollToEnd,
    onSelectBook,
  };
}

function getHasAdditionalItems(data: RemoteData<GetBooksResponse>, pageSize: number): boolean {
  if (isRemoteDataLoading(data)) { return true; }
  if (isRemoteDataError(data)) { return false; }
  return (pageSize < data.value.count);
}

function extractPaginatedItems(data: RemoteData<GetBooksResponse>): RemoteData<Array<Book>> {
  if (isRemoteDataSuccess(data)) { return remoteDataSuccess(data.value.items); }
  if (isRemoteDataLoading(data)) { return remoteDataLoading(data.placeholder && data.placeholder.items); }
  return data;
}
