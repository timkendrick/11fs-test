import React from 'react';
import { View } from 'react-native';
import { BookList } from '../../components/BookList';
import { Book } from '../../types/api/Book';
import { RemoteData } from '../../types/RemoteData';
import styles from './HomeScreen.styles';

export interface HomeScreenViewProps {
  books: RemoteData<Array<Book>>;
  hasAdditionalItems?: boolean;
  onScrollToEnd?: () => void;
  onSelectBook?: (book: Book) => void;
}

export const HomeScreenView = React.memo(function HomeScreenView(props: HomeScreenViewProps): JSX.Element {
  const { books, onSelectBook, hasAdditionalItems, onScrollToEnd } = props;
  return (
    <View style={styles.root}>
      <BookList
        style={styles.list}
        books={books}
        hasAdditionalItems={hasAdditionalItems}
        onScrollToEnd={onScrollToEnd}
        onSelectBook={onSelectBook}
      />
    </View>
  );
});
