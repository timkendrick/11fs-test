import Chance from 'chance';
import loremIpsum from 'lorem-ipsum';
import { Author } from '../types/api/Author';
import { Book } from '../types/api/Book';
import { Edition } from '../types/api/Edition';
import { Format } from '../types/api/Format';
import { Publisher } from '../types/api/Publisher';
import { Review } from '../types/api/Review';
import { User } from '../types/api/User';
import { Uuid } from '../types/api/Uuid';

interface IntegerRange {
  between: number;
  and: number;
}

const generate = new Chance();

export function generateRandomData(options: {
  numBooks: IntegerRange,
  numEditionsPerBook: IntegerRange,
  numReviewsPerBook: IntegerRange,
  numAuthors: IntegerRange,
  numPublishers: IntegerRange,
  numUsers: IntegerRange,
  formatNames: Array<string>,
}): {
  authors: Array<Author>,
  books: Array<Book>,
  editions: Array<Edition>,
  formats: Array<Format>,
  publishers: Array<Publisher>,
  users: Array<User>,
  reviews: Array<Review>,
} {
  const { numBooks, numEditionsPerBook, numReviewsPerBook, numAuthors, numPublishers, numUsers, formatNames } = options;
  const authors: Array<Author> = randomLengthRange(numAuthors).map(() => createEntity({
    name: randomName(),
  }));
  const publishers: Array<Publisher> = randomLengthRange(numPublishers).map(() => createEntity({
    name: randomCompany(),
  }));
  const formats: Array<Author> = formatNames.map((name) => createEntity({ name }));
  const users: Array<User> = randomLengthRange(numUsers).map(() => createEntity({
    name: randomName(),
  }));
  const editions: Array<Edition> = [];
  const reviews: Array<Review> = [];
  const books = randomLengthRange(numBooks).map(() => generateRandomBook({
    numEditionsPerBook,
    numReviewsPerBook,
    authors,
    publishers,
    formats,
    users,
    editions,
    reviews,
  }));
  return {
    authors,
    books,
    editions,
    formats,
    publishers,
    users,
    reviews,
  };
}

function generateRandomBook(options: {
  numEditionsPerBook: IntegerRange,
  numReviewsPerBook: IntegerRange,
  authors: Array<Author>,
  editions: Array<Edition>,
  formats: Array<Format>,
  publishers: Array<Publisher>,
  users: Array<User>,
  reviews: Array<Review>,
}): Book {
  const { numEditionsPerBook, numReviewsPerBook, authors, editions, formats, publishers, users, reviews } = options;
  const averageRating = randomInt(2, 5);
  return createEntity({
    title: randomBookTitle(),
    description: randomBookDescription(),
    author: randomArrayItem(authors),
    publisher: randomArrayItem(publishers),
    editions: randomArraySubset(formats, numEditionsPerBook).map((format) => addToArray(editions, createEntity({
      format,
      isbn: randomIsbn(),
    }))),
    reviews: randomLengthRange(numReviewsPerBook).map(() => addToArray(reviews, createEntity({
      user: randomArrayItem(users),
      rating: randomRating(averageRating),
      title: randomReviewTitle(),
      body: randomReviewBody(),
    }))),
  });
}

function randomName(): string {
  return `${generate.first()} ${generate.last()}`;
}

function randomBookTitle(): string {
  return titleCase(loremIpsum({ units: 'words', count: randomInt(1, 6) }));
}

function randomBookDescription(): string {
  return loremIpsum({ units: 'paragraphs', count: 1 });
}

function randomIsbn(): string {
  return [3, 1, 2, 6, 1].map((length) => range(0, length).map(randomDigit).join('')).join('-');
}

function randomDigit(): number {
  return randomInt(0, 10);
}

function randomCompany(): string {
  return generate.company();
}

function randomReviewTitle(): string {
  return titleCase(loremIpsum({ units: 'words', count: randomInt(5, 10) }));
}

function randomReviewBody(): string {
  return loremIpsum({
    units: 'paragraphs',
    count: 1,
    paragraphLowerBound: 2,
    paragraphUpperBound: 5,
  });
}

function randomRating(averageRating: number): number {
  return randomInt(Math.max(1, averageRating - 1), Math.min(6, averageRating + 1));
}

function randomLengthRange(options: IntegerRange): Array<number> {
  const { between: min, and: max } = options;
  const length = randomInt(min, max);
  return range(0, length);
}

function randomInt(min: number, max: number): number {
  return Math.floor(min + Math.random() * (max - min));
}

function titleCase(value: string): string {
  return value.replace(/\b\w/g, (char) => char.toUpperCase());
}

function randomArrayItem<T>(values: Array<T>): T {
  return values[Math.floor(Math.random() * values.length)];
}

function randomArraySubset<T>(values: Array<T>, length: number | IntegerRange): Array<T> {
  const numItems = (typeof length === 'number' ? length : randomInt(length.between, length.and));
  const remainingValues = values.slice();
  const items: Array<T> = [];
  while ((items.length < numItems) && (remainingValues.length > 0)) {
    items.push(remainingValues.splice(Math.floor(Math.random() * remainingValues.length), 1)[0]);
  }
  return items;
}

function range(offset: number, length: number, step: number = 1): Array<number> {
  return Array.from({ length }, (_, index) => offset + (index * step));
}

function addToArray<T>(values: Array<T>, value: T): T {
  values.push(value);
  return value;
}

function createEntity<P extends {}>(properties: P): P & { id: Uuid } {
  return {
    id: generate.guid(),
    ...properties,
  };
}
