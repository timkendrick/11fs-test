import { Book } from '../types/api/Book';
import { Uuid } from '../types/api/Uuid';
import { delay } from '../utils/lib/delay';
import { generateRandomData } from './generateRandomData';

const SIMULATED_LATENCY = 1000;
const NUM_BOOKS = 3000;

const data = generateRandomData({
  numBooks: { between: NUM_BOOKS, and: NUM_BOOKS },
  numEditionsPerBook: { between: 1, and: 5 },
  numReviewsPerBook: { between: 0, and: 10 },
  numAuthors: { between: NUM_BOOKS / 2, and: NUM_BOOKS / 2 },
  numPublishers: { between: 20, and: 50 },
  numUsers: { between: NUM_BOOKS, and: NUM_BOOKS * 2 },
  formatNames: ['Paperback', 'Hardback', 'Audiobook', 'Kindle'],
});

export function getBooks(options: { offset: number, length: number }): Promise<{
  items: Array<Book>,
  count: number,
}> {
  const { offset, length } = options;
  return delay(SIMULATED_LATENCY).then(() => ({
    items: data.books.slice(offset, offset + length),
    count: data.books.length,
  }));
}

export function getBook(id: Uuid): Promise<Book> {
  return delay(SIMULATED_LATENCY).then(() => {
    const book = data.books.find((book) => book.id === id);
    if (!book) { throw new Error('Book not found'); }
    return book;
  });
}
