import { useState } from 'react';
import { Book } from '../../types/api/Book';
import { Omit } from '../../types/Omit';
import { HigherOrderComponent } from '../../types/React';
import { isRemoteDataLoading, isRemoteDataSuccess } from '../../types/RemoteData';
import { escapeRegExpSource } from '../../utils/lib/escapeRegExpSource';
import { withProps } from '../../utils/withProps';
import { BookListViewProps } from './BookList.view';

export interface BookListContainerProps extends Omit<BookListViewProps, 'filter' | 'setFilter'> {
}

export const BookListContainer: HigherOrderComponent<
  BookListContainerProps,
  BookListViewProps
> = withProps(assignViewProps);

function assignViewProps(props: BookListContainerProps): BookListViewProps {
  const [filter, setFilter] = useState('');
  return {
    ...props,
    books: (
      isRemoteDataSuccess(props.books)
      ? { ...props.books, value: filterBooks(props.books.value, filter) }
      : isRemoteDataLoading(props.books)
      ? { ...props.books, placeholder: props.books.placeholder && filterBooks(props.books.placeholder, filter) }
      : props.books
    ),
    filter,
    setFilter,
    hasAdditionalItems: filter ? undefined : props.hasAdditionalItems,
    onScrollToEnd: filter ? undefined : props.onScrollToEnd,
  };
}

function filterBooks(books: Array<Book>, filter: string): Array<Book> {
  const pattern = new RegExp(escapeRegExpSource(filter), 'i');
  // TODO: Implement fuzzy-filtering, sorted according to relevance, while ensuring stable sort
  return books.filter((book) => pattern.test(book.title) || pattern.test(book.author.name));
}
