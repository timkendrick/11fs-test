import React from 'react';
import {
  FlatList,
  GestureResponderEvent,
  ListRenderItem,
  ListRenderItemInfo,
  StyleProp,
  Text,
  TextInput,
  View,
  ViewStyle,
} from 'react-native';
import { Book } from '../../types/api/Book';
import { RemoteData } from '../../types/RemoteData';
import { RemoteError } from '../../types/RemoteError';
import { ErrorDisplay } from '../../ui/ErrorDisplay';
import { ListItemSeparator } from '../../ui/ListItemSeparator';
import { ListLoadingIndicator } from '../../ui/ListLoadingIndicator';
import { LoadingIndicator } from '../../ui/LoadingIndicator';
import { Touchable } from '../../ui/Touchable';
import { MemoizerFn, useMemoizer } from '../../utils/hooks/useMemoizer';
import { remoteDataView } from '../../utils/lib/remoteDataView';
import styles from './BookList.styles';

export interface BookListViewProps {
  style?: StyleProp<ViewStyle>;
  books: RemoteData<Array<Book>>;
  filter?: string;
  setFilter?: (value: string) => void;
  hasAdditionalItems?: boolean;
  onScrollToEnd?: () => void;
  onSelectBook?: (value: Book) => void;
}

export interface MemoizedProps {
  memoize: MemoizerFn;
  memoizeLatest: MemoizerFn;
}

const render = remoteDataView((props: BookListViewProps & MemoizedProps) => props.books, {
  error: renderError,
  loading: renderLoading,
  success: renderSuccess,
});

export const BookListView = React.memo(function BookListView(props: BookListViewProps): JSX.Element {
  return render({
    ...props,
    memoize: useMemoizer(),
    memoizeLatest: useMemoizer({ latest: true }),
  });
});

function renderView(props: BookListViewProps & MemoizedProps, children: React.ReactChild): JSX.Element {
  return (
    <View style={styles.root}>
      {children}
    </View>
  );
}

function renderError(error: RemoteError, props: BookListViewProps & MemoizedProps): JSX.Element {
  return renderView(props, <ErrorDisplay error={error} />);
}

function renderLoading(placeholder: Array<Book> | undefined, props: BookListViewProps & MemoizedProps): JSX.Element {
  if (placeholder) { return renderList(placeholder, props); }
  return renderView(props, <LoadingIndicator />);
}

function renderSuccess(value: Array<Book>, props: BookListViewProps & MemoizedProps): JSX.Element {
  return renderList(value, props);
}

function renderList(value: Array<Book>, props: BookListViewProps & MemoizedProps): JSX.Element {
  const {
    books,
    setFilter,
    hasAdditionalItems,
    onScrollToEnd,
    onSelectBook,
    memoize,
    memoizeLatest,
  } = props;
  return renderView(props, (
    <FlatList
      style={styles.list}
      data={value}
      keyExtractor={getListItemKey}
      ListHeaderComponent={memoize(createListHeaderComponent)(setFilter)}
      renderItem={memoizeLatest(createListItemRenderer)(onSelectBook)}
      ItemSeparatorComponent={ListItemSeparator}
      ListFooterComponent={books.loading || hasAdditionalItems ? ListLoadingIndicator : undefined}
      onEndReached={onScrollToEnd}
      onEndReachedThreshold={1}
    />
  ));
}

function getListItemKey(value: Book, index: number): string {
  return value.id;
}

function createListHeaderComponent(setFilter: BookListViewProps['setFilter']): React.ComponentType<{}> {
  return React.memo(() => (
    <TextInput
      key="filter"
      style={styles.filter}
      placeholder="Filter by title or author…"
      clearButtonMode="always"
      clearTextOnFocus
      autoCorrect={false}
      autoCapitalize="none"
      onChangeText={setFilter}
    />
  ));
}

function createListItemRenderer(
  onSelectItem: ((value: Book) => void) | undefined,
): ListRenderItem<Book> {
  const component = React.memo(createListItemComponent(onSelectItem));
  return (info: ListRenderItemInfo<Book>): JSX.Element => React.createElement(component, info);
}

function createListItemComponent(
  onSelectItem: ((value: Book) => void) | undefined,
): React.ComponentType<ListRenderItemInfo<Book>> {
  if (onSelectItem) {
    return withListItemPressHandler(
      (info: ListRenderItemInfo<Book>) => { onSelectItem(info.item); },
    )(renderListItem);
  } else {
    return renderListItem;
  }
}

function withListItemPressHandler<T>(
  handler: (info: ListRenderItemInfo<T>, event: GestureResponderEvent) => void,
): (renderer: ListRenderItem<T>) => ListRenderItem<T> {
  return (renderer: ListRenderItem<T>): ListRenderItem<T> => (
    (info: ListRenderItemInfo<T>): JSX.Element => (
      <Touchable style={styles.button} onPress={(event) => handler(info, event)}>
        {renderer(info)}
      </Touchable>
    )
  );
}

function renderListItem(info: ListRenderItemInfo<Book>): JSX.Element {
  const { item: book } = info;
  return (
    <View style={styles.item}>
      <Text style={styles.title}>{book.title}</Text>
      <Text style={styles.author}>{book.author.name}</Text>
    </View>
  );
}
