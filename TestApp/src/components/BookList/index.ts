import { BookListContainer, BookListContainerProps } from './BookList.container';
import { BookListView } from './BookList.view';

export * from './BookList.view';
export * from './BookList.container';
export const BookList = BookListContainer(BookListView);
export interface BookListProps extends BookListContainerProps {}
