import { StyleSheet } from 'react-native';
import {
  BORDER_STYLE_SECTION_SEPARATOR,
  LIST,
  LIST_ITEM,
  LIST_ITEM_SUBTITLE,
  LIST_ITEM_TITLE,
  SEARCH_INPUT,
} from '../../styles';

export default StyleSheet.create({
  root: {
    flex: 1,
  },
  list: {
    flex: 1,
    ...LIST,
  },
  filter: {
    ...SEARCH_INPUT,
    ...BORDER_STYLE_SECTION_SEPARATOR,
  },
  button: {},
  item: LIST_ITEM,
  title: LIST_ITEM_TITLE,
  author: LIST_ITEM_SUBTITLE,
});
