import { BookDetailContainer, BookDetailContainerProps } from './BookDetail.container';
import { BookDetailView } from './BookDetail.view';

export * from './BookDetail.view';
export * from './BookDetail.container';
export const BookDetail = BookDetailContainer(BookDetailView);
export interface BookDetailProps extends BookDetailContainerProps {}
