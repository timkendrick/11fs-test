import React from 'react';
import { ScrollView, StyleProp, Text, View, ViewStyle } from 'react-native';
import { Book } from '../../types/api/Book';
import { Review } from '../../types/api/Review';
import { RemoteData } from '../../types/RemoteData';
import { RemoteError } from '../../types/RemoteError';
import { ErrorDisplay } from '../../ui/ErrorDisplay';
import { LoadingIndicator } from '../../ui/LoadingIndicator';
import { Rating } from '../../ui/Rating';
import { remoteDataView } from '../../utils/lib/remoteDataView';
import styles from './BookDetail.styles';

export interface BookDetailViewProps {
  style?: StyleProp<ViewStyle>;
  book: RemoteData<Book>;
}

export const BookDetailView = React.memo(remoteDataView(
  (props: BookDetailViewProps) => props.book,
  {
    error: renderError,
    loading: renderLoading,
    success: renderSuccess,
  },
));

function renderView(props: BookDetailViewProps, children: React.ReactChild): JSX.Element {
  return (
    <ScrollView style={styles.root}>
      {children}
    </ScrollView>
  );
}

function renderError(error: RemoteError, props: BookDetailViewProps): JSX.Element {
  return renderView(props, <ErrorDisplay error={error} />);
}

function renderLoading(placeholder: Book | undefined, props: BookDetailViewProps): JSX.Element {
  if (placeholder) { return renderDetail(placeholder, props); }
  return renderView(props, <LoadingIndicator />);
}

function renderSuccess(book: Book, props: BookDetailViewProps): JSX.Element {
  return renderDetail(book, props);
}

function renderDetail(book: Book, props: BookDetailViewProps): JSX.Element {
  const rating = getOverallRating(book.reviews);
  return renderView(props, (
    <View style={styles.content}>
      <View style={styles.header}>
        <Text style={styles.title}>{book.title}</Text>
        <Text style={styles.author}>{book.author.name}</Text>
        {rating === undefined
          ? null
          : <View style={styles.titleReviews}>
            <Rating style={styles.titleRating} value={rating} />
            <Text style={styles.titleReviewCount}>({formatNumReviews(book.reviews.length)})</Text>
          </View>
        }
      </View>
      <Text style={styles.description}>{book.description}</Text>
      <View style={styles.separator} />
      <View style={styles.details}>
        <Text style={styles.detailsHeader}>Additional information</Text>
        <View style={styles.detailsGrid}>
          <Text style={styles.detailHeader}>Publisher</Text>
          <Text style={styles.detailValue}>{book.publisher.name}</Text>
          <Text style={styles.detailHeader}>Available formats</Text>
          {book.editions.map((edition) => (
            <Text key={`format:${edition.format.id}`} style={styles.detailValue}>
              {edition.format.name}
            </Text>
          ))}
          <Text style={styles.detailHeader}>ISBN</Text>
          {book.editions.map((edition, index, editions) => (
            <Text key={`isbn:${edition.format.id}`} style={styles.detailValue}>
              {edition.isbn}{editions.length > 1 ? ` (${edition.format.name})` : null}
            </Text>
          ))}
        </View>
      </View>
      <View style={styles.separator} />
      <View style={styles.reviews}>
        <Text style={styles.reviewsHeader}>Latest reviews</Text>
        {book.reviews.length > 0
          ? book.reviews.slice(0, 3).map(renderReview)
          : <Text style={styles.reviewsInfo}>No recent reviews</Text>
        }
      </View>
    </View>
  ));
}

function renderReview(review: Review): JSX.Element {
  return (
    <View key={review.id} style={styles.review}>
      <View style={styles.reviewHeader}>
        <Rating style={styles.reviewRating} value={review.rating} />
        <Text style={styles.reviewTitle}>{review.title}</Text>
      </View>
      <Text style={styles.reviewBody}>{review.body}</Text>
    </View>
  );
}

function getOverallRating(reviews: Array<Review>): number | undefined {
  if (reviews.length === 0) { return undefined; }
  return reviews.reduce((acc, review) => acc + review.rating, 0) / reviews.length;
}

function formatNumReviews(count: number): React.ReactChild {
  if (count === 1) { return '1 review'; }
  return `${count} reviews`;
}
