import { StyleSheet } from 'react-native';
import {
  BODY_TEXT,
  BOLD_TEXT,
  HEADER,
  HEADER_SUBTITLE,
  HEADER_TITLE,
  LIST_ITEM_TITLE,
  PADDING_DEFAULT,
  PADDING_SUBTITLE,
  PAGE,
  SCROLL_CONTENT,
  SECTION,
  SECTION_SEPARATOR,
  SECTION_TITLE,
} from '../../styles';

export default StyleSheet.create({
  root: {
    flex: 1,
    ...PAGE,
  },
  content: SCROLL_CONTENT,
  header: HEADER,
  title: HEADER_TITLE,
  author: HEADER_SUBTITLE,
  titleReviews: {
    flexDirection: 'row',
    marginTop: 0.5 * PADDING_DEFAULT,
  },
  titleRating: {
    flex: 0,
    marginTop: 4,
    marginRight: 0.5 * PADDING_DEFAULT,
  },
  titleReviewCount: {
    flex: 1,
    marginTop: 2,
    ...BODY_TEXT,
  },
  description: BODY_TEXT,
  separator: SECTION_SEPARATOR,
  details: SECTION,
  detailsHeader: SECTION_TITLE,
  detailsGrid: {
    marginTop: 0.5 * PADDING_DEFAULT,
  },
  detailHeader: {
    ...BOLD_TEXT,
    marginTop: 0.5 * PADDING_DEFAULT,
    marginBottom: PADDING_SUBTITLE,
  },
  detailValue: {
    ...BODY_TEXT,
    marginBottom: 0.5 * PADDING_DEFAULT,
  },
  reviews: SECTION,
  reviewsInfo: BODY_TEXT,
  reviewsHeader: SECTION_TITLE,
  review: {
    marginTop: PADDING_DEFAULT,
    marginBottom: PADDING_DEFAULT,
  },
  reviewHeader: {
    flexDirection: 'row',
    marginBottom: 0.5 * PADDING_DEFAULT,
  },
  reviewRating: {
    flex: 0,
    marginTop: 4,
    marginRight: 8,
  },
  reviewTitle: {
    flex: 1,
    ...LIST_ITEM_TITLE,
  },
  reviewBody: BODY_TEXT,
});
