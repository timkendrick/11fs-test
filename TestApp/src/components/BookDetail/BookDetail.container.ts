import { HigherOrderComponent } from '../../types/React';
import { identity } from '../../utils/lib/identity';
import { BookDetailViewProps } from './BookDetail.view';

export interface BookDetailContainerProps extends BookDetailViewProps {
}

export const BookDetailContainer: HigherOrderComponent<
  BookDetailContainerProps,
  BookDetailViewProps
> = identity;
