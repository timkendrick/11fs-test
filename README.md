# 11:FS Technical Task: Take 2

## Installation

```bash
yarn
```

## Launching the project

```bash
# Start the packager
yarn start:app

# Launch the simulator
yarn start:ios
```

## Notes

- The reason it's set up using Yarn workspaces is because I was originally planning to add a separate server package for the GraphQL side of things, but I didn't manage to get round to it. The API is currently faked on the client side (there's a simple promise-based API that returns dummy data after a simulated delay). The fake API currently only returns complete objects, rather that just the fields needed for that particular view, however I did build in a mechanism so that partial field sets could be used as temporary loading placeholders while the full content loads.
- I've used a fairly functional style, mainly to mimic the `recompose`-style code used in the interview – it's a fun exercise, but I'm not sure how I would feel about it for a production project, partly because it makes the control flow less straightforward and partly because TypeScript still isn't great at figuring out the types of curried functions that use generics.
- Async data is loaded in via React Hooks; this was the first time I've properly used them so still need to figure out the best way to debounce these to prevent unnecessary fetches
- I haven't gone as far as I would have liked on the `shouldComponentUpdate` side of things. All the view components are wrapped in `React.memo()`, but this will still result in unnecessary renders when e.g. a parent component uses a higher-order 'callback factory' function to create a partially-applied function that is passed down to a child component. Repeated renders will produce different callback functions every time, even when called with the same partial arguments. My usual approach to these scenarios is to use old-fashioned `React.PureComponent` class instances alongside something like my [`memoize-bind`](https://www.npmjs.com/package/memoize-bind) package, which allows you to to maintain a per-instance cache of the memoized callbacks. The would let you pass down partially-applied functions to children and avoid unnecessary re-renders, without having to worry about potential memory leaks like you would with a global memoizer. With the SFC/hooks approach however, there's no component instance to use as the primary cache key, so you have to resort to using something like instructing the `useCallback` hook to regenerate the partially applied function whenever the dependencies change. This breaks the encapsulation of the function, and has the potential to get very messy very quickly. My main issue with using hooks for memoization is that the Rules of Hooks mean you effectively have to define all your hooks at the top of the render function before any branching, so you have to hoist all your partial application stuff to the top of the render function, regardless of whether it makes sense for it to be there. I need to have a bit more of a play with it to figure out if there's a more elegant way to do this.
